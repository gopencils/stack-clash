﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public bool isResetData;
    public bool isSwapPlayerHole;
    public string mapID;


    [Header("Status")]
    public int levelGame;
    public int levelFixed;
    public bool isComplete;
    private bool isChangeColor;
    public int turn;


    [Header("Level Controller")]
    public int count;
    public int maxCount;
    public bool isMoving;
    public bool isScored;
    public List<Ball> ballList;
    public Ball player;
    public List<Brick> brickList = new List<Brick>();
    private List<GameObject> obtacleList = new List<GameObject>();
    public List<GameObject> blankList = new List<GameObject>();
    public Texture2D[] textures;
    private Vector4 Purple = new Vector4(138, 0, 255, 255);
    private Vector4 Red = new Vector4(255, 0, 0, 255);
    private Vector4 Black = new Vector4(0, 0, 0, 255);
    private Vector4 White = new Vector4(255, 255, 255, 255);
    private Vector4 Green = new Vector4(12, 255, 0, 255);
    private Vector4 Boss = new Vector4(197, 0, 0, 255);
    public bool isBlack;
    public GameObject emoji;
    public Animator lineAnimator;
    public GameObject lineParent;
    public GameObject holeObject;
    public float maxHeightSandWich;
    public Transform parentRedBall;
    public Transform parentYellowBall;
    public Transform left, right;
    public Transform parentWhite;
    public List<GameObject> listBoss = new List<GameObject>();
    public bool isBoss = false;
    public int currentboss;

    [Header("Score Controller")]
    public int totalMoney;
    public int combo;
    public int swipeAmount;
    public int point;
    public List<int> scores = new List<int>();

    [Header("Camera Controller")]
    public GameObject pivotCamera;
    public float _horizontal;
    public float _pivotHorizontal;

    [Header("UI")]
    public Animator tutorialAnimator;
    public Text currentLevelText;
    public Text nextLevelText;
    public Text swipeAmountText;
    public Text score;
    public Text comboText;
    public Animator comboAnimator;
    public Animator scoreAnimatior;
    public GameObject combo01;
    public Text combo01Text;
    public string[] congratulations;
    public Image levelFill;
    public GameObject eatTextObject;
    public GameObject yourTurn;
    public GameObject enemyTurn;
    public GameObject bossLevel;

    public delegate void DisableCollider();
    public static event TouchSwipe disableCollider;

    public delegate void EnableCollider();
    public static event TouchSwipe enableCollider;

    public delegate void TouchSwipe();
    public static event TouchSwipe touchSwipe;

    public Vector3 directionSwipe;

    [Header("Star Manager")]
    public int currentMove;
    public int maxMove;
    public Text currentMoveText;
    public Text maxMoveText;

    public Material brickMaterial;
    public Material obtacleMaterial;

    [Header("Tutorial")]

    public List<GameObject> tutorialStep = new List<GameObject>();

    private void Awake()
    {
        if (isResetData) PlayerPrefs.DeleteAll();

        if(PlayerPrefs.GetInt("first") == 0)
        {
            PlayerPrefs.SetInt("first", 1);
            PlayerPrefs.SetInt("model", 8);
        }

        Application.targetFrameRate = 60;
        MMVibrationManager.iOSInitializeHaptics();

        instance = this;
    }

    private void Start()
    {
        totalMoney = PlayerPrefs.GetInt("TotalMoney", 0);
        levelGame = PlayerPrefs.GetInt("levelGame", 0);
        currentLevelText.text = "" + (levelGame + 1);
        nextLevelText.text = "" + (levelGame + 2);
        levelFixed = levelGame;
        if (levelFixed >= textures.Length)
        {
            levelFixed = Random.Range(0, textures.Length);
        }
        Debug.Log(levelGame);
        if(levelGame < 4)
        {
            tutorialStep[levelGame].SetActive(true);
        }
        GenerateLevel();
        isStartGame = true;
        turn = 0;
    }

    private void Update()
    {
        if (isStartGame && turn == 0)
        {
            if (Input.GetMouseButton(0))
            {
                OnMouseDrag();
            }

            if (Input.GetMouseButtonUp(0))
            {
                OnMouseUp();
            }
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            PlayerPrefs.DeleteAll();
        }
    }

    public GameObject firstHit;
    public RaycastHit[] hits;
    public List<Transform> listHit = new List<Transform>();
    public bool isStartGame = false;
    public bool isControl = true;
    bool isDrag = false;
    bool isVibrate = false;
    public float speed;
    public LayerMask dragMask;
    public List<int> listValueSpawn = new List<int>();
    public List<Color> listColor = new List<Color>();
    public int currentTask;
    public int moves;
    public static int bonus;
    public GameObject conffeti;
    public GameObject blast;
    public List<Color> envColor = new List<Color>();
    public List<GameObject> envMap = new List<GameObject>();
    public GameObject floor;
    public GameObject dieEffect;
    public List<GameObject> listChoose = new List<GameObject>();
    public List<GameObject> listNotChoose = new List<GameObject>();
    public Transform lastHit = null;
    public GameObject flag;
    public LayerMask ballmask;

    void OnMouseDrag()
    {
        isDrag = true;
        if (levelGame < 4)
        {
            tutorialStep[levelGame].SetActive(false);
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        var screenPoint = Input.mousePosition;
        screenPoint.z = 12.0f;

        if (Physics.Raycast(ray, out hit, 1000, dragMask))
        {
            if (hit.transform.CompareTag("Brick"))
            {
                if (lastHit != null && lastHit != hit.transform)
                {
                    lastHit.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = lastHit.GetComponent<Brick>().defaultColor;
                    if (listChoose.Count > 0)
                    {
                        foreach (var item in listChoose)
                        {
                            item.GetComponent<QuickOutline>().enabled = false;
                        }
                        listChoose.Clear();
                    }
                }
                lastHit = hit.transform;
                hit.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.yellow;
                flag.SetActive(true);
                flag.transform.position = new Vector3(hit.transform.position.x, -0.5f, hit.transform.position.z);
                for (int i = 0; i < ballList.Count; i++)
                {
                    if (ballList[i] == null)
                    {
                        ballList.RemoveAt(i);
                    }
                }
                listChoose.Clear();
                bool isValid = false;
                foreach (var item in ballList)
                {
                    if(item == null)
                    {
                        break;
                    }
                    RaycastHit detect;
                    if (Physics.Raycast(hit.transform.position, hit.transform.right, out detect, 1000, ballmask))
                    {
                        if (item.GetComponent<Ball>().ballType == Ball.BallType.black)
                        {
                            isValid = true;
                        }
                        listChoose.Add(detect.transform.gameObject);
                    }
                    if (Physics.Raycast(hit.transform.position, -hit.transform.right, out detect, 1000, ballmask))
                    {
                        if (item.GetComponent<Ball>().ballType == Ball.BallType.black)
                        {
                            isValid = true;
                        }
                        listChoose.Add(detect.transform.gameObject);
                    }
                    if (Physics.Raycast(hit.transform.position, hit.transform.forward, out detect, 1000, ballmask))
                    {
                        if (item.GetComponent<Ball>().ballType == Ball.BallType.black)
                        {
                            isValid = true;
                        }
                        listChoose.Add(detect.transform.gameObject);
                    }
                    if (Physics.Raycast(hit.transform.position, -hit.transform.forward, out detect, 1000, ballmask))
                    {
                        if (item.GetComponent<Ball>().ballType == Ball.BallType.black)
                        {
                            isValid = true;
                        }
                        listChoose.Add(detect.transform.gameObject);
                    }
                }
                if (listChoose.Count > 0 && isValid)
                {
                    foreach (var item in listChoose)
                    {
                        item.GetComponent<QuickOutline>().enabled = true;
                    }
                }
                else
                {
                    listChoose.Clear();
                }
            }
        }
    }

    int lastMove = -1;
    int moveC = 0;
    void OnMouseUp()
    {
        if (isDrag)
        {
            moveC = 0;
            if (listChoose.Count > 0)
            {
                foreach (var item in listChoose)
                {
                    StartCoroutine(move(item, new Vector3(lastHit.position.x, 0.6f, lastHit.position.z)));
                    moveC++;
                }
                StartCoroutine(delayTurn());
            }
            firstHit = null;
            isDrag = false;
            if(lastHit != null)
            lastHit.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = lastHit.GetComponent<Brick>().defaultColor;
            //flag.SetActive(false);
            listChoose.Clear();
        }
    }

    IEnumerator delayTurn()
    {
        while (moveC > 0)
        {
            //Debug.Log(moveC);
            yield return null;
        }
        //Debug.Log(moveC);
        yield return new WaitForSeconds(0.2f);
        for (int i = 0; i < ballList.Count; i++)
        {
            if (ballList[i] == null)
            {
                ballList.RemoveAt(i);
            }
        }
        yield return new WaitForSeconds(0.2f);
        int redCount = 0;
        int blackCount = 0;
        foreach (var item in ballList)
        {
            if (item != null)
            {
                if (item.GetComponent<Ball>().ballType == Ball.BallType.black)
                {
                    blackCount++;
                }
                if (item.GetComponent<Ball>().ballType == Ball.BallType.red)
                {
                    redCount++;
                }
            }
        }
        //Debug.Log(redCount + " " + blackCount);
        if (redCount == 0)
        {
            turn = 2;
            UIManager.instance.Show_CompleteUI();
        }
        if (blackCount == 0)
        {
            turn = 2;
            UIManager.instance.Show_FailUI();
        }
        if (turn == 0)
        {
            turn = 1;
            enemyTurn.SetActive(true);
            yourTurn.SetActive(false);
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(botMove());
        }
        else if(turn == 1)
        {
            turn = 0;
            yourTurn.SetActive(true);
            enemyTurn.SetActive(false);
        }
        yield return new WaitForSeconds(1.5f);
        //yourTurn.SetActive(false);
        //enemyTurn.SetActive(false);
    }

    public Transform botPivot;
    Vector3 tempMove;
    public List<Vector3> listBotMove = new List<Vector3>();
    IEnumerator botMove()
    {
        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < ballList.Count; i++)
        {
            if (ballList[i] == null)
            {
                ballList.RemoveAt(i);
            }
        }
        for (int u = 0; u < ballList.Count; u++)
        {
            for (int j = 0; j < ballList.Count; j++)
            {
                if(u != j && ballList[u].GetComponent<Ball>().ballType == Ball.BallType.red)
                {
                    if (ballList[j].GetComponent<Ball>().ballType == Ball.BallType.black)
                    {
                        if (ballList[u].transform.position.x == ballList[j].transform.position.x || ballList[u].transform.position.z == ballList[j].transform.position.z)
                        {
                            tempMove = ballList[j].transform.position + (ballList[u].transform.position - ballList[j].transform.position).normalized;
                            listBotMove.Add(tempMove);
                            while (botPivot.transform.position != tempMove)
                            {
                                botPivot.transform.position = Vector3.MoveTowards(botPivot.transform.position, tempMove, Random.Range(3, 10) * Time.deltaTime);
                                botDrag(botPivot.transform.position);
                                yield return null;
                            }
                            break;
                        }
                        else
                        {
                            if (ballList[u].transform.position.x > ballList[j].transform.position.z)
                            {
                                tempMove = new Vector3(ballList[u].transform.position.x, ballList[u].transform.position.y, ballList[j].transform.position.z);
                            }
                            else if (ballList[u].transform.position.z > ballList[j].transform.position.x)
                            {
                                tempMove = new Vector3(ballList[j].transform.position.x, ballList[u].transform.position.y, ballList[u].transform.position.z);
                            }
                            listBotMove.Add(tempMove);
                            while (botPivot.transform.position != tempMove)
                            {
                                botPivot.transform.position = Vector3.MoveTowards(botPivot.transform.position, tempMove, Random.Range(3, 10) * Time.deltaTime);
                                botDrag(botPivot.transform.position);
                                yield return null;
                            }
                            break;
                        }
                    }
                    //else
                    //{
                    //    if (ballList[u].transform.position.x == ballList[j].transform.position.x || ballList[u].transform.position.z == ballList[j].transform.position.z)
                    //    {
                    //        tempMove = ballList[j].transform.position + (ballList[u].transform.position - ballList[j].transform.position).normalized;
                    //        listBotMove.Add(tempMove);
                    //        while (botPivot.transform.position != tempMove)
                    //        {
                    //            botPivot.transform.position = Vector3.MoveTowards(botPivot.transform.position, tempMove, Random.Range(3, 10) * Time.deltaTime);
                    //            botDrag(botPivot.transform.position);
                    //            yield return null;
                    //        }
                    //        break;
                    //    }
                    //    else
                    //    {
                    //        if (ballList[u].transform.position.x > ballList[j].transform.position.z)
                    //        {
                    //            tempMove = new Vector3(ballList[u].transform.position.x, ballList[u].transform.position.y, ballList[j].transform.position.z);
                    //        }
                    //        else if (ballList[u].transform.position.z > ballList[j].transform.position.x)
                    //        {
                    //            tempMove = new Vector3(ballList[j].transform.position.x, ballList[u].transform.position.y, ballList[u].transform.position.z);
                    //        }
                    //        listBotMove.Add(tempMove);
                    //        while (botPivot.transform.position != tempMove)
                    //        {
                    //            botPivot.transform.position = Vector3.MoveTowards(botPivot.transform.position, tempMove, Random.Range(3,10) * Time.deltaTime);
                    //            botDrag(botPivot.transform.position);
                    //            yield return null;
                    //        }
                    //        break;
                    //    }
                    //}
                }
            }
        }
        if(listChoose.Count < 1)
        {
            while (botPivot.transform.position != listBotMove[0])
            {
                botPivot.transform.position = Vector3.MoveTowards(botPivot.transform.position, listBotMove[0], Random.Range(3, 10) * Time.deltaTime);
                botDrag(botPivot.transform.position);
                yield return null;
            }
        }
        botDecided();
    }

    public void botDecided()
    {
        moveC = 0;
        if (listChoose.Count > 0)
        {
            foreach (var item in listChoose)
            {
                StartCoroutine(move(item, new Vector3(lastHit.position.x, 0.6f, lastHit.position.z)));
                moveC++;
            }
            StartCoroutine(delayTurn());
        }
        else
        {
            UIManager.instance.Show_CompleteUI();
        }
        lastHit.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = lastHit.GetComponent<Brick>().defaultColor;
        //flag.SetActive(false);
        listChoose.Clear();
    }

    public void botDrag(Vector3 move)
    {
        RaycastHit hit;
        Ray ray = new Ray(move, Vector3.down);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, dragMask))
        {
            if (hit.transform.CompareTag("Brick")/* && hit.transform.GetChild(0).CompareTag("Plus")*/)
            {
                if (lastHit != null && lastHit != hit.transform)
                {
                    lastHit.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = lastHit.GetComponent<Brick>().defaultColor;
                    if (listChoose.Count > 0)
                    {
                        foreach (var item in listChoose)
                        {
                            item.GetComponent<QuickOutline>().enabled = false;
                        }
                        listChoose.Clear();
                    }
                }
                lastHit = hit.transform;
                hit.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.yellow;
                flag.SetActive(true);
                flag.transform.position = new Vector3(hit.transform.position.x, -0.5f, hit.transform.position.z);
                for (int i = 0; i < ballList.Count; i++)
                {
                    if (ballList[i] == null)
                    {
                        ballList.RemoveAt(i);
                    }
                }
                listChoose.Clear();
                foreach (var item in ballList)
                {
                    if (item == null)
                    {
                        break;
                    }
                    RaycastHit detect;
                    if (Physics.Raycast(hit.transform.position, hit.transform.right, out detect, 1000, ballmask))
                    {
                        listChoose.Add(detect.transform.gameObject);
                    }
                    if (Physics.Raycast(hit.transform.position, -hit.transform.right, out detect, 1000, ballmask))
                    {
                        listChoose.Add(detect.transform.gameObject);
                    }
                    if (Physics.Raycast(hit.transform.position, hit.transform.forward, out detect, 1000, ballmask))
                    {
                        listChoose.Add(detect.transform.gameObject);
                    }
                    if (Physics.Raycast(hit.transform.position, -hit.transform.forward, out detect, 1000, ballmask))
                    {
                        listChoose.Add(detect.transform.gameObject);
                    }
                }
                if (listChoose.Count > 0)
                {
                    foreach (var item in listChoose)
                    {
                        item.GetComponent<QuickOutline>().enabled = true;
                    }
                }
            }
        }
    }

    IEnumerator move(GameObject moveObject, Vector3 des)
    {
        moveObject.GetComponent<Ball>().currentDes = des;
        yield return new WaitForSeconds(0.01f);
        //if (!moveObject.transform.GetChild(moveObject.transform.childCount - 1).GetChild(1).GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("10_Running 0"))
        try
        {
            if (moveObject != null || moveObject.GetComponent<Ball>().mainChar != null)
            {
                if (!isBoss)
                    moveObject.GetComponent<Ball>().mainChar.GetComponent<Animator>().Play("10_Running 0");
                else
                    moveObject.GetComponent<Ball>().mainChar.GetComponent<Animator>().Play("Cast Spell");
            }
        }
        catch { }
        while (moveObject != null && moveObject.transform.position != des)
        {
            //var dir = des - transform.position;
            //dir.y = 0;
            moveObject.GetComponent<Ball>().isMoving = true;
            moveObject.transform.position = Vector3.MoveTowards(moveObject.transform.position, des, 1f*Time.deltaTime);
            if (moveObject.GetComponent<Ball>().mainChar != null)
            {
                moveObject.GetComponent<Ball>().mainChar.transform.LookAt(des);
                Vector3 eulerAngles = moveObject.GetComponent<Ball>().mainChar.transform.localRotation.eulerAngles;
                eulerAngles.x = 0;
                eulerAngles.z = 0;
                moveObject.GetComponent<Ball>().mainChar.transform.localRotation = Quaternion.Euler(eulerAngles);
                //moveObject.GetComponent<Ball>().mainChar.transform.rotation = Quaternion.Slerp(moveObject.GetComponent<Ball>().mainChar.transform.rotation, Quaternion.LookRotation(dir), 30 * Time.deltaTime);
            }
            yield return null;
        }
        moveC--;
        //if (moveObject != null)
        //{
        //    if(moveObject.transform.childCount <= 1)
        //    {
        //        moveObject.GetComponent<Ball>().UpdateCharacterStatus();
        //    }
        //    //moveObject.transform.GetChild(moveObject.transform.childCount - 1).GetChild(1).GetComponent<Animator>().Play("09_Jump 0");
        //}
        if (moveObject != null)
        {           
            moveObject.GetComponent<Ball>().isMoving = false;
            if (moveObject.GetComponent<Ball>().isContact)
            {
                Destroy(moveObject);
            }
            else
            {
                //if (moveObject.transform.childCount > 1)
                    moveObject.GetComponent<Ball>().ReCheck();
            }
        }
    }

    public void LevelUp()
    {
        var random = Random.value;
        if (random < 0.65)
        {
            random = 0.65f;
        }

        if (swipeAmount != 0)
            totalMoney += (((int)(point * maxCount * 1.5 * random)) / swipeAmount);

        levelGame++;

        PlayerPrefs.SetInt("levelGame", levelGame);

        levelFixed = levelGame;

        if (levelFixed >= textures.Length)
        {
            //levelFixed = Random.Range(0, textures.Length);
            levelFixed = levelGame % textures.Length;
        }

        isChangeColor = true;
    }

    public void LevelDown()
    {
        var random = Random.value;
        if (random < 0.65)
        {
            random = 0.65f;
        }

        if (swipeAmount != 0)
            totalMoney += (((int)(point * maxCount * 1.5 * random)) / swipeAmount);

        levelGame--;
        if(levelGame < 0)
        {
            levelGame = textures.Length - 1;
        }    

        PlayerPrefs.SetInt("levelGame", levelGame);

        levelFixed = levelGame;

        if (levelFixed >= textures.Length)
        {
            //levelFixed = Random.Range(0, textures.Length);
            levelFixed = levelGame % textures.Length;
        }

        isChangeColor = true;
    }

    public int _d;
    public int _maxD;

    public void ActiveColliderBrick()
    {
        _d++;
        if (_d >= _maxD)
        {
            enableCollider();
            _d = 0;
            _maxD = 0;
        }
    }

    #region Map
    private void ClearMap()
    {
        for(int i = 0; i < ballList.Count; i++)
        {
            if (ballList[i].ballType == Ball.BallType.red)
            {
                ballList[i].transform.SetParent(parentRedBall);
            }
            else
            {
                ballList[i].transform.SetParent(parentYellowBall);
            }
        }

        Vector3 _posCamMain = Camera.main.transform.localPosition;
        _posCamMain.y = 16;
        _posCamMain.z = -7f;
        Camera.main.transform.localPosition = _posCamMain;

        pivotCamera.transform.eulerAngles = Vector3.zero;
        _d = _maxD = 0;
        //levelFill.fillAmount = 0;
        blankObjects.Clear();
        ballList.Clear();
        brickList.Clear();
        obtacleList.Clear();
        blankList.Clear();
        swipeAmount = 0;
        //swipeAmountText.text = swipeAmount.ToString();
        isComplete = false;
        count = maxCount = point = 0;
        Time.timeScale = 1;
        isComplete = false;
        parentWhite.position = Vector3.zero;
        PoolManager.instance.RefreshItem(PoolManager.NameObject.brick);
        PoolManager.instance.RefreshItem(PoolManager.NameObject.tile);
        PoolManager.instance.RefreshItem(PoolManager.NameObject.redball);
        PoolManager.instance.RefreshItem(PoolManager.NameObject.blackball);
        PoolManager.instance.RefreshItem(PoolManager.NameObject.hole);
        PoolManager.instance.RefreshItem(PoolManager.NameObject.obstacle);
    }

    public void GenerateLevel()
    {
        //AnalyticsManager.instance.CallEvent(AnalyticsManager.EventType.StartEvent);

        ClearMap();

        if (isChangeColor)
        {
            isChangeColor = false;
            ColorManager.instance.ChangeColor();
        }

        levelFixed = levelGame % textures.Length;

        UIManager.instance.Show_InGameUI();
        currentLevelText.text = "" + (levelFixed + 1);
        nextLevelText.text = "" + (levelFixed + 2);
        GenerateMap(textures[levelFixed]);
    }

    public void ResetLevel()
    {
        levelGame = 0;
        PlayerPrefs.SetInt("levelGame", levelGame);
        GenerateLevel();

    }

    private void GenerateMap(Texture2D texture)
    {
        mapID = texture.name;
        char c = mapID.Last();
        currentboss = int.Parse(c.ToString());
        if(mapID.Contains("Boss"))
        {
            isBoss = true;
            bossLevel.SetActive(true);
            Destroy(bossLevel, 2);
        }
        if (texture.height % 2 == 0)
        {
            for (int x = 0; x < texture.width; x++)
            {
                isBlack = !isBlack;
                for (int y = 0; y < texture.height; y++)
                {
                    GenerateTile(texture, x, y);
                    isBlack = !isBlack;
                }
            }

        }
        else if (texture.height % 2 != 0)
        {

            for (int x = 0; x < texture.width; x++)
            {
                for (int y = 0; y < texture.height; y++)
                {
                    GenerateTile(texture, x, y);
                    isBlack = !isBlack;
                }
            }
        }

        SetCamera(texture);

        for (int y = -10; y <= texture.height + 30; y++)
        {
            for (int x = -10; x <= texture.width + 10; x++)
            {
                bool _x = false;
                bool _y = false;

                if (x > 0 && x < texture.width - 1)
                {
                    _x = true;
                }

                if (y > 0 && y < texture.height - 1)
                {
                    _y = true;
                }

                GameObject _obj = PoolManager.instance.GetObject(PoolManager.NameObject.white);
                _obj.transform.position = new Vector3(x, -1, y);

                if (_x && _y)
                {
                    _obj.SetActive(true);
                    blankList.Add(_obj);
                }
                else
                {
                    _obj.SetActive(true);
                }

                _obj.GetComponent<Renderer>().material.color = ColorManager.instance.currentSetColor.plane;
            }
        }

        for(int i = 0; i < blankList.Count; i++)
        {
            blankList[i].gameObject.SetActive(false);
        }
    }


    public IEnumerator ResetTimeScale()
    {
        yield return new WaitForSeconds(0.25f);
        Time.timeScale = 1f;
    }
    private void GenerateTile(Texture2D texture, int x, int y)
    {
        Color32 pixelColor = texture.GetPixel(x, y);
        Vector4 color32 = new Vector4(pixelColor.r, pixelColor.g, pixelColor.b, pixelColor.a);
        Vector3 pos = new Vector3(x, 0, y);

        if (color32 == Red)
        {
            GenerateRedBall(pos);
            GenerateBrick(pos);
        }
        else if (color32 == Purple)
        {
            GenerateObstacle(pos);
        }
        else if (color32 == Black)
        {
            GenerateBlackBall(pos);
            GenerateBrick(pos);
        }
        else if (color32 == White)
        {
            GenerateBrick(pos);
        }
        else if (color32 == Green)
        {
            GenerateHole(pos);
        }
        else if(color32 == Boss)
        {
            GenerateBoss(pos);
            GenerateBrick(pos);
        }
    }

    private void GenerateBlackBall(Vector3 _position)
    {
        GameObject yellowBallObject = PoolManager.instance.GetObject(PoolManager.NameObject.blackball);
        var ball = yellowBallObject.GetComponent<Ball>();
        _position.y = 0.7f;
        ball.Init(_position);
        ballList.Add(ball);
        maxCount++;
        yellowBallObject.SetActive(true);
    }

    private void GenerateRedBall(Vector3 _position)
    {
        GameObject redBallObject = PoolManager.instance.GetObject(PoolManager.NameObject.redball);
        player = redBallObject.GetComponent<Ball>();
        _position.y = 0.7f;
        player.Init(_position);
        ballList.Add(player);
        maxCount++;
        redBallObject.SetActive(true);
    }

    private void GenerateBoss(Vector3 _position)
    {
        GameObject redBallObject = PoolManager.instance.GetObject(PoolManager.NameObject.redball);
        player = redBallObject.GetComponent<Ball>();
        _position.y = 0.7f;
        player.Init(_position);
        ballList.Add(player);
        maxCount++;
        redBallObject.SetActive(true);
        for (int i = 0; i < 5; i++)
        {
            var dup = Instantiate(redBallObject.transform.GetChild(0), redBallObject.transform);
            dup.transform.DOLocalMoveY(dup.transform.GetSiblingIndex(), 0);
        }
        redBallObject.tag = "Boss";
        player.ReCheck();
    }

    public void GenerateHole(Vector3 _position)
    {
        GameObject obstacleObject = PoolManager.instance.GetObject(PoolManager.NameObject.hole);
        _position.y = -0.2f;
        //obstacleObject.transform.localScale = new Vector3(1, 1.5f, 1);
        obstacleObject.transform.position = _position;
        obstacleObject.SetActive(true);
        //obstacleObject.GetComponent<Renderer>().material = obtacleMaterial;
        obtacleList.Add(obstacleObject);
    }

    private void GenerateObstacle(Vector3 _position)
    {
        GameObject obstacleObject = PoolManager.instance.GetObject(PoolManager.NameObject.obstacle);
        _position.y = 0f;
        //obstacleObject.transform.localScale = new Vector3(1, 1.5f, 1);
        obstacleObject.transform.position = _position;
        obstacleObject.SetActive(true);
        //obstacleObject.GetComponent<Renderer>().material = obtacleMaterial;
        obtacleList.Add(obstacleObject);
    }

    public void GenerateBrick(Vector3 _position)
    {
        GameObject brickObject = PoolManager.instance.GetObject(PoolManager.NameObject.brick);
        _position.y = 0f;
        brickObject.transform.position = _position;
        var brick = brickObject.GetComponent<Brick>();
        // brickObject.layer = 10;
        brickList.Add(brickObject.GetComponent<Brick>());
        brickObject.SetActive(true);
        if (_position.z % 2 == 0)
        {
            if (_position.x % 2 == 0)
            {
                brickObject.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = ColorManager.instance.currentSetColor.caro1;
                brickObject.GetComponent<Brick>().defaultColor = ColorManager.instance.currentSetColor.caro1;
            }
            else
            {
                brickObject.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = ColorManager.instance.currentSetColor.caro2;
                brickObject.GetComponent<Brick>().defaultColor = ColorManager.instance.currentSetColor.caro2;
            }
        }
        else
        {
            if (_position.x % 2 == 0)
            {
                brickObject.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = ColorManager.instance.currentSetColor.caro2;
                brickObject.GetComponent<Brick>().defaultColor = ColorManager.instance.currentSetColor.caro2;
            }
            else
            {
                brickObject.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = ColorManager.instance.currentSetColor.caro1;
                brickObject.GetComponent<Brick>().defaultColor = ColorManager.instance.currentSetColor.caro1;
            }
        }
    }

    #endregion

    public void SetPoint(int ballPoint)
    {
        point += ballPoint;
        swipeAmountText.text = point.ToString();
    }

    public void UpdateLevelBar ()
    {
        levelFill.fillAmount = ((float)count / (float)maxCount);
    }

    public void CheckComplete()
    {
        if (count >= maxCount)
        {
            Complete();
        }
    }

    int index;
    public void ChangeModel()
    {
        index++;

        if (index >= 3) index = 0;

        for(int i = 0; i < ballList.Count; i++)
        {
            ballList[i].ChangeModel(index);
        }
    }

    private void ShowBlank()
    {
        for(int i = 0; i < brickList.Count; i++)
        {
            brickList[i].gameObject.SetActive(false);
        }

        for(int i = 0; i < obtacleList.Count; i++)
        {
            obtacleList[i].gameObject.SetActive(false);
        }

        for(int i = 0; i < blankList.Count; i++)
        {
            blankList[i].gameObject.SetActive(true);
        }
    }

    public void Complete()
    {
        if (isComplete) return;

        isComplete = true;
        StartCoroutine(C_Complete());
    }

    private IEnumerator C_Complete()
    {
        yield return new WaitForSeconds(1.0f);
        LevelUp();

        //yield return C_CameraSnap();
        ShowBlank();

        // PoolManager.instance.RefreshItem(PoolManager.NameObject.brick);
        // PoolManager.instance.RefreshItem(PoolManager.NameObject.tile);
        // PoolManager.instance.RefreshItem(PoolManager.NameObject.obstacle);

        yield return new WaitForSeconds(0.25f);

        Vector3 _eulerAngle = lineParent.transform.localEulerAngles;

        int r = Random.Range(0, 4);

        if(r == 0)
        {
            _eulerAngle.y = 45;
        }
        else if ( r== 1)
        {
            _eulerAngle.y = 45 * 3;
        }
        else if (r == 2)
        {
            _eulerAngle.y = 45 * 5;
        }
        else
        {
            _eulerAngle.y = 45 * 7;
        }

        //AnalyticsManager.instance.CallEvent(AnalyticsManager.EventType.EndEvent);

        score.text = totalMoney.ToString();

        Debug.Log("Complete");


        yield return new WaitForSeconds(1.25f);

        UIManager.instance.Show_CompleteUI();
      //  starControl.SetActive(true);
    }

    public void Fail()
    {
        if (isComplete) return;

        isComplete = true;
        StartCoroutine(C_Fail());
    }

    private IEnumerator C_Fail()
    {
        //AnalyticsManager.instance.CallEvent(AnalyticsManager.EventType.EndEvent);

        Debug.Log("Fail");

        yield return new WaitForSeconds(1.25f);

        UIManager.instance.Show_FailUI();
    }

    private bool isVibrating;

    public void Vibration()
    {
        if (isVibrating) return;

        StartCoroutine(C_Vibration());
    }

    private IEnumerator C_Vibration()
    {
        // Debug.Log("vibration");

        MMVibrationManager.iOSTriggerHaptics(HapticTypes.LightImpact);

        isVibrating = true;

        yield return new WaitForSeconds(0.1f);

        isVibrating = false;
    }

    public List<GameObject> blankObjects;
    public Vector3[][] blankArray;
   
    private void SetCamera(Texture2D texture)
    {
        float _posX = (texture.width-1)/ 2.0f;
        float _posY = (texture.height-1) / 2.0f;
        pivotCamera.transform.position = new Vector3(_posX, 0.0f, _posY);

        _horizontal = texture.width - 2;
        _pivotHorizontal = _horizontal / 2.0f;
        CameraControl.instance.SetFOVSizeMap();
    }
}
