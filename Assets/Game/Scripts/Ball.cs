﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{
    public GameObject[] models;

    private bool isHide;
    public Rigidbody rigidbody;
    public Collider collider;

    public LayerMask layer;
    public LayerMask layerBrick;

    public BallType ballType;

    public GameObject[] elementModel;
    public bool isContact = false;

    public enum BallType
    {
        red,
        black
    }

    public Color redColor; 
    public Color blackColor;
    public Color currentColor;
    public bool isMoving;
    public Collider colliderBrick;
    public bool isRemoveEvent;
    public Vector3 currentDes;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
    }

    private void OnEnable()
    {
        StopAllCoroutines();

        isRemoveEvent = false;

        GameManager.enableCollider += ActiveColliderBrick;
        GameManager.disableCollider += DisableColliderBrick;
    }

    private void Start()
    {
        //UpdateCharacterStatus();
        StartCoroutine(delayStart());
    }

    IEnumerator delayStart()
    {
        yield return new WaitForSeconds(0.1f);
        UpdateCharacterStatus();
    }

    private void OnDisable()
    {
        if(isRemoveEvent == false)
        {
            isRemoveEvent = true;
            GameManager.enableCollider -= ActiveColliderBrick;
            GameManager.disableCollider -= DisableColliderBrick;
        }
    }

    public void Init(Vector3 _position)
    {
        isCompleted = false;
        isHide = false;
        colliderBrick = null;
        transform.eulerAngles = Vector3.zero;
        rigidbody.useGravity = false;
        rigidbody.velocity = Vector3.zero;
        collider.isTrigger = true;

        collider.isTrigger = true;
        collider.enabled = true;
    
        transform.position = _position;
    }

    public Collider[] colliders;

    public void ActiveColliderBrick()
    {
        if (colliderBrick != null)
        {
            colliderBrick.enabled = true;

            if (isHide)
            {
                isHide = false;

                if (isRemoveEvent == false)
                {
                    isRemoveEvent = true;
                    GameManager.enableCollider -= ActiveColliderBrick;
                    GameManager.disableCollider -= DisableColliderBrick;
                }
            }
        }
    }

    public void DisableColliderBrick()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position + Vector3.up * 10, Vector3.down);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerBrick))
        {
            if (hit.collider != null)
            {
                colliderBrick = hit.collider.gameObject.GetComponent<Collider>();
                colliderBrick.enabled = false;
            }
        }
    }

    private bool isCompleted;

    public void Completed()
    {
        isCompleted = true;

        GameManager.enableCollider -= ActiveColliderBrick;
        GameManager.disableCollider -= DisableColliderBrick;
    }

    public void Hide()
    {
        int currentPoint = (int)Mathf.Pow(2f, GameManager.instance.combo);

        GameManager.instance.scores.Add(currentPoint);

        GameManager.instance.SetPoint(currentPoint);
        GameManager.instance.combo++;

        isHide = true;
        isMoving = false;

        GameManager.instance.ActiveColliderBrick();
        transform.SetParent(GameManager.instance.holeObject.transform.GetChild(0).GetChild(0).GetChild(0));
    }

    public void ChangeModel(int index)
    {      
        for(int i = 0; i < models.Length; i++)
        {    
            models[i].SetActive(false);
        }

        models[index].SetActive(true);
    }

    public GameObject mainChar;
    public void UpdateCharacterStatus()
    {
        //PoolManager.instance.RefreshItem(PoolManager.NameObject.character);
        //Debug.Log("Spawn");
        var count = transform.childCount - 1;
        if (mainChar == null)
        {
            if (!GameManager.instance.isBoss)
                mainChar = PoolManager.instance.GetObject(PoolManager.NameObject.character);
            else
            {
                Debug.Log("True");
                if (CompareTag("Boss"))
                    mainChar = Instantiate(GameManager.instance.listBoss[GameManager.instance.currentboss]);
                else
                    mainChar = PoolManager.instance.GetObject(PoolManager.NameObject.character);
            }
        }
        if (mainChar != null)
        {
            mainChar.transform.parent = transform.GetChild(count).transform;
            mainChar.transform.localPosition = new Vector3(0, 0.5f, 0);

            if (ballType == BallType.red)
            {
                if (!GameManager.instance.isBoss)
                {
                    var mats = mainChar.GetComponentsInChildren<SkinnedMeshRenderer>();
                    foreach (var item in mats)
                    {
                        item.material.color = Color.yellow;
                    }
                }
            }
            else if (ballType == BallType.black)
            {
                var mats = mainChar.GetComponentsInChildren<SkinnedMeshRenderer>();
                foreach (var item in mats)
                {
                    item.material.color = Color.green;
                }
            }
            mainChar.SetActive(true);
            if (transform.GetChild(count).childCount > 2)
            {
                Destroy(transform.GetChild(count).GetChild(2).gameObject);
            }
            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    if (transform.GetChild(i).childCount > 1)
                    {
                        Destroy(transform.GetChild(i).GetChild(1).gameObject);
                    }
                }
            }
            if (!GameManager.instance.isBoss)
            {
                if (!isMoving)
                    mainChar.GetComponent<Animator>().Play("09_Jump 0");
                else
                    mainChar.GetComponent<Animator>().Play("10_Running 0");
            }
            else
            {
                if (!isMoving)
                    //    mainChar.GetComponent<Animator>().Play("Idle");
                    //else
                    mainChar.GetComponent<Animator>().Play("Cast Spell");
            }
        }
    }

    private bool isJump;
    private float JumpProgress;

    private IEnumerator JumpCoroutine(Vector3 startPosition ,Vector3 destination, float maxHeight, float time,float timefixed)
    {
        JumpProgress = 0.0f;

        isJump = true;
        var startPos = startPosition;


        float distance_xyz = Vector3.Distance(startPos, destination);

        float he = distance_xyz / 5;
        maxHeight *= he;

        float ti = distance_xyz / 8;
        time *= ti;

        while (JumpProgress <= 1.0)
        {
            JumpProgress += Time.deltaTime * timefixed;
            var height = Mathf.Sin(Mathf.PI * JumpProgress) * maxHeight;
            if (height < 0f)
            {
                height = 0f;
            }

            if(ballType == BallType.red)
            {
                if (JumpProgress >= 0.6f)
                {
                    // GameManager.instance.SlowMotion();
                }
            }
        
            transform.position = Vector3.Lerp(startPos, destination, JumpProgress) + Vector3.up * height;
            yield return null;
        }

        transform.position = destination;
        isJump = false;
    }

    public void Jump()
    {
        Vector3 _pos = transform.position;
        float _force = transform.position.y / 2.0f;
        float _duration = 0.25f + transform.position.y / 10.0f;

        transform.DOJump(_pos, _force, 1, _duration);
    }

    IEnumerator C_Check()
    {
        while(isMoving)
        {
            yield return null;
        }
        yield return new WaitForSeconds(0.05f);
        //Debug.LogError(transform.childCount);
        transform.GetChild(transform.childCount - 1).GetComponentInChildren<Text>().text = transform.childCount.ToString();
        UpdateCharacterStatus();
    }

    public void ReCheck()
    {
        //transform.GetChild(transform.childCount - 1).GetComponentInChildren<Text>().text = transform.childCount.ToString();
        StartCoroutine(C_Check());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Plus") && isMoving)
        {
            //Debug.Log("Plus");
            var dup = Instantiate(transform.GetChild(0), transform);
            dup.transform.DOLocalMoveY(dup.transform.GetSiblingIndex(), 0);
            transform.GetChild(transform.childCount - 1).GetComponentInChildren<Text>().text = transform.childCount.ToString();
            UpdateCharacterStatus();
            //other.tag = "Hole";
        }
        if (other.CompareTag("Hole") && isMoving)
        {
            try
            {
                var count = transform.childCount - 1;
                if (count > 0)
                {
                    if (count < transform.childCount)
                    {
                        var total = transform.childCount - count;
                        for (int i = transform.childCount - 1; i >= count; i--)
                        {
                            Destroy(transform.GetChild(i).gameObject);
                        }
                    }
                }
                else
                {
                    if (ballType == BallType.red)
                    {
                        currentColor = redColor;
                    }
                    else
                    {
                        currentColor = blackColor;
                    }
                    GameObject _obj1 = PoolManager.instance.GetObject(PoolManager.NameObject.blackExplosion);
                    _obj1.transform.position = (gameObject.transform.position + other.gameObject.transform.position) / 2;
                    _obj1.SetActive(true);
                    _obj1.GetComponent<ParticleSystem>().startColor = currentColor;
                    if (mainChar != null)
                    {
                        if (!GameManager.instance.isBoss)
                            mainChar.GetComponent<Animator>().Play("Fall");
                        else
                            mainChar.GetComponent<Animator>().Play("Die");
                        var dir2 = -mainChar.transform.forward;
                        //dir2 *= 2;
                        Debug.Log(dir2);
                        //dir2 = dir2.normalized;
                        mainChar.transform.DOKill();
                        mainChar.transform.DOMoveY(0.5f, 1);
                        if ((int)dir2.x > 1)
                            dir2.x = 1;
                        else if ((int)dir2.x < -1)
                            dir2.x = -1;
                        other.GetComponent<Ball>().mainChar.transform.DOLocalMoveX((int)dir2.x, 1);
                        if ((int)dir2.z > 1)
                            dir2.z = 1;
                        else if ((int)dir2.x < -1)
                            dir2.x = -1;
                        other.GetComponent<Ball>().mainChar.transform.DOLocalMoveZ((int)dir2.z, 1);
                        mainChar.transform.parent = null;
                        Destroy(mainChar, 1);
                    }
                    GetComponent<BoxCollider>().enabled = false;
                    Destroy(gameObject, 0.05f);
                }
                transform.GetChild(transform.childCount - 1).GetComponentInChildren<Text>().text = transform.childCount.ToString();
                UpdateCharacterStatus();
                ReCheck();
            }
            catch { }
            //other.tag = "Plus";
        }
        if (other.CompareTag("Obstacle") && isMoving)
        {
            if (ballType == BallType.red)
            {
                currentColor = redColor;
            }
            else
            {
                currentColor = blackColor;
            }
            var count = transform.childCount - 2;
            if (count > 0)
            {
                if (count < transform.childCount)
                {
                    var total = transform.childCount - count;
                    //for (int i = transform.childCount - 1; i >= count; i--)
                    //{
                    //    Destroy(transform.GetChild(i).gameObject);
                    //}
                    for (int i = 0; i < total; i++)
                    {
                        Destroy(transform.GetChild(i).gameObject, 0.05f);
                        GameObject _obj1 = PoolManager.instance.GetObject(PoolManager.NameObject.blackExplosion);
                        _obj1.transform.position = (transform.GetChild(i).gameObject.transform.position + other.gameObject.transform.position) / 2;
                        _obj1.SetActive(true);
                        _obj1.GetComponent<ParticleSystem>().startColor = currentColor;
                    }
                    foreach (Transform item in transform)
                    {
                        StartCoroutine(delayDrop(item));
                    }
                }
            }
            else
            {
                GameObject _obj1 = PoolManager.instance.GetObject(PoolManager.NameObject.blackExplosion);
                _obj1.transform.position = (gameObject.transform.position + other.gameObject.transform.position) / 2;
                _obj1.SetActive(true);
                _obj1.GetComponent<ParticleSystem>().startColor = currentColor;
                if (mainChar != null)
                {
                    if (!GameManager.instance.isBoss)
                        mainChar.GetComponent<Animator>().Play("Fall");
                    else
                        mainChar.GetComponent<Animator>().Play("Die");
                    var dir2 = -mainChar.transform.forward;
                    //dir2 *= 2;
                    Debug.Log(dir2);
                    //dir2 = dir2.normalized;
                    mainChar.transform.DOKill();
                    mainChar.transform.DOMoveY(0.5f, 1);
                    if ((int)dir2.x > 1)
                        dir2.x = 1;
                    else if ((int)dir2.x < -1)
                        dir2.x = -1;
                    other.GetComponent<Ball>().mainChar.transform.DOLocalMoveX((int)dir2.x, 1);
                    if ((int)dir2.z > 1)
                        dir2.z = 1;
                    else if ((int)dir2.x < -1)
                        dir2.x = -1;
                    other.GetComponent<Ball>().mainChar.transform.DOLocalMoveZ((int)dir2.z, 1);
                    mainChar.transform.parent = null;
                    Destroy(mainChar, 1);
                }
                GetComponent<BoxCollider>().enabled = false;
                Destroy(gameObject, 0.05f);
            }
            transform.GetChild(transform.childCount - 1).GetComponentInChildren<Text>().text = transform.childCount.ToString();
            UpdateCharacterStatus();
            ReCheck();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Plus") && isMoving)
        {
            GameManager.instance.GenerateHole(other.transform.position);
            other.GetComponent<BoxCollider>().enabled = false;
            Destroy(other.transform.parent.gameObject, 0.05f);
            //other.tag = ("Hole");
        }
        if (other.CompareTag("Hole") && isMoving)
        {
            GameManager.instance.GenerateBrick(other.transform.position);
            other.GetComponent<BoxCollider>().enabled = false;
            Destroy(other.transform.parent.gameObject, 0.05f);
            //other.tag = ("Plus");
        }
    }

    IEnumerator delayDrop(Transform target)
    {
        yield return new WaitForSeconds(0.25f);
        if(target != null)
        target.transform.DOLocalMoveY(target.transform.GetSiblingIndex(), 0.1f);
    }

    private void OnTriggerStay(Collider other)
    {
        if ((other.CompareTag("Ball") || other.CompareTag("Boss")) && !isContact && !other.GetComponent<Ball>().isContact)
        {
            //Destroy(other.gameObject, 1f);
            other.GetComponent<Ball>().isContact = true;
            if (other.GetComponent<Ball>().ballType == ballType)
            {
                var countOther = other.transform.childCount;
                for (int i = 0; i < countOther; i++)
                {
                    var dup = Instantiate(transform.GetChild(0), transform);
                    dup.transform.DOLocalMoveY(dup.transform.GetSiblingIndex(), 0f);
                }
            }
            else
            {
                int count = 0;
                var countOther = other.transform.childCount;
                if (countOther > transform.childCount)
                {
                    //isContact = true;
                    count = countOther - transform.childCount;
                    ballType = other.GetComponent<Ball>().ballType;
                    Color colorChange = redColor;
                    if(ballType == BallType.black)
                    {
                        colorChange = blackColor;
                    }
                    foreach (Transform child in transform)
                    {
                        child.GetComponent<MeshRenderer>().materials[0].color = colorChange;
                    }
                }
                else
                {
                    //other.GetComponent<Ball>().isContact = true;
                    count = transform.childCount - countOther;
                }
                if (count > 0)
                {
                    if (count > transform.childCount)
                    {
                        var dup = Instantiate(transform.GetChild(0), transform);
                        dup.transform.DOLocalMoveY(dup.transform.GetSiblingIndex(), 0);
                    }
                    else if (count < transform.childCount)
                    {
                        var total = transform.childCount - count;
                        //for (int i = transform.childCount - 1; i >= count; i--)
                        //{
                        //    Destroy(transform.GetChild(i).gameObject);
                        //}
                        for (int i = 0; i < total; i++)
                        {
                            Destroy(transform.GetChild(i).gameObject, 0.05f);
                        }
                        foreach(Transform item in transform)
                        {
                            StartCoroutine(delayDrop(item));
                        }
                    }
                }
                else
                {
                    if(ballType == BallType.red)
                    {
                        currentColor = redColor;
                    }
                    else
                    {
                        currentColor = blackColor;
                    }
                    GameObject _obj1 = PoolManager.instance.GetObject(PoolManager.NameObject.blackExplosion);
                    _obj1.transform.position = (gameObject.transform.position + other.gameObject.transform.position) / 2;
                    _obj1.SetActive(true);
                    _obj1.GetComponent<ParticleSystem>().startColor = currentColor;
                    if (mainChar != null)
                    {
                        if (!GameManager.instance.isBoss)
                            mainChar.GetComponent<Animator>().Play("Fall");
                        else
                            mainChar.GetComponent<Animator>().Play("Die");
                        var dir2 = other.transform.forward;
                        //var dir2 = transform.position - other.transform.position;
                        //dir2 = dir2.normalized;
                        //dir2 *= 2;
                        mainChar.transform.DOKill();
                        mainChar.transform.DOMoveY(0.5f, 1);
                        if ((int)dir2.x != 0)
                            mainChar.transform.DOLocalMoveX((int)dir2.x, 1);
                        if ((int)dir2.x != 0)
                            mainChar.transform.DOLocalMoveZ((int)dir2.z, 1);
                        mainChar.transform.parent = null;
                        Destroy(mainChar, 1);
                    }
                    GetComponent<BoxCollider>().enabled = false;
                    Destroy(gameObject, 0.05f);
                }
                if (ballType == BallType.red)
                {
                    currentColor = blackColor;
                }
                else
                {
                    currentColor = redColor;
                }
                GameObject _obj2 = PoolManager.instance.GetObject(PoolManager.NameObject.blackExplosion);
                _obj2.transform.position = (gameObject.transform.position + other.gameObject.transform.position) / 2;
                _obj2.SetActive(true);
                _obj2.GetComponent<ParticleSystem>().startColor = currentColor;
                if (other.GetComponent<Ball>().mainChar != null)
                {
                    if (!GameManager.instance.isBoss)
                        other.GetComponent<Ball>().mainChar.GetComponent<Animator>().Play("Fall");
                    //else
                    //    other.GetComponent<Ball>().mainChar.GetComponent<Animator>().Play("Die");
                    //var dir = -other.GetComponent<Ball>().mainChar.transform.forward;
                    var dir = other.transform.forward;
                    //dir = dir.normalized;
                    //dir *= 2;
                    other.GetComponent<Ball>().mainChar.transform.DOKill();
                    other.GetComponent<Ball>().mainChar.transform.DOMoveY(0.5f, 1);
                    if ((int)dir.x != 0)
                        other.GetComponent<Ball>().mainChar.transform.DOLocalMoveX((int)dir.x, 1);
                    if ((int)dir.x != 0)
                        other.GetComponent<Ball>().mainChar.transform.DOLocalMoveZ((int)dir.z, 1);
                    other.GetComponent<Ball>().mainChar.transform.parent = null;
                    if (GameManager.instance.isBoss)
                        Destroy(other.GetComponent<Ball>().mainChar, 0);
                    else
                        Destroy(other.GetComponent<Ball>().mainChar, 1);
                }
                if(other.CompareTag("Boss"))
                {
                    tag = "Boss";
                }
                other.GetComponent<BoxCollider>().enabled = false;
                Destroy(other.gameObject, 0.05f);
            }
            transform.GetChild(transform.childCount - 1).GetComponentInChildren<Text>().text = transform.childCount.ToString();
            //if (mainChar != null)
            //    mainChar.transform.parent = null;
            UpdateCharacterStatus();
            ReCheck();
        }
    }
}
