﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorManager : MonoBehaviour
{
    public static ColorManager instance;

    private void Awake()
    {
        instance = this;
        RandomCurrentNumber();
    }

    [Header("References")]
    public List<SetColor> listSetColor = new List<SetColor>();
    public SetColor currentSetColor;
    private int currentNumber;

    [System.Serializable]
    public class SetColor
    {
        public Color caro1;

        public Color caro2;

        public Color obtacle;

        public Color plane;
    }

    private void Start()
    {
        //RandomCurrentNumber();
    }

    private void RandomCurrentNumber()
    {
        for (int i = 0; i < 1; i++)
        {
            int r = Random.Range(0, listSetColor.Count);

            if(r == currentNumber)
            {
                i--;
            }
            else
            {
                currentNumber = r;
            }
        }

        currentSetColor = listSetColor[currentNumber];
    }

    private void NextCurrentNumber()
    {
        currentNumber++;

        if (currentNumber >= listSetColor.Count)
        {
            currentNumber = 0;
        }

        currentSetColor = listSetColor[currentNumber];
    }

    public void ChangeColor()
    {
        //NextCurrentNumber();
    }
}
